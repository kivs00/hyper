import { createStore } from 'vuex';

export default createStore({
  state: {
    currentUser: {
      id: 42,
      avatarUrl:
        'https://cdn.discordapp.com/avatars/898109824523440129/2a79acaa5be2e9485baf1c1024a6e5dc.png?size=128',
      username: 'Хупа',
      discordUsername: 'hyperspace42#9872',
      about:
        'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima ad tempore provident quod tenetur',
      trending: 100,
      status: 'Разработчик',
      cash: 100,
      freezedCash: 381,
      friends: 3,
      roles: ['Разработчик', 'Админ', 'Игрок'],
    },

    allUsers: [
      {
        id: 42,
        avatarUrl:
          'https://cdn.discordapp.com/avatars/898109824523440129/2a79acaa5be2e9485baf1c1024a6e5dc.png?size=128',
        username: 'Хупа',
        discordUsername: 'hyperspace42#9872',
        about:
          'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima ad tempore provident quod tenetur',
        trending: 100,
        status: 'Разработчик',
        cash: 100,
        freezedCash: 381,
        friends: 3,
        roles: ['Разработчик', 'Админ', 'Игрок'],
      },
      {
        id: 2,
        avatarUrl:
          'https://cdn.discordapp.com/avatars/898109824523440129/2a79acaa5be2e9485baf1c1024a6e5dc.png?size=128',
        username: 'Абобыч',
        discordUsername: 'bebrich#3162',
        about:
          'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima ad tempore provident quod tenetur',
        trending: 89,
        status: 'Игрок',
        cash: 66,
        freezedCash: 590,
        friends: 2,
        roles: ['Игрок'],
      },
    ],

    allUsersRequests: [
      {
        id: 1,
        authorId: 42,
        requestAuthorUsername: 'hyperspace42#9872',
        requestType: 'Услуга',
        requestPrice: 125,
        requestDescription: 'Разнообразный и бога развития',
        requestTime: '17:32 10.08.2021',
        status: 'Ожидание',
        performerId: 0,
        performerUsername: '',
        completed: false
      },
      {
        id: 2,
        authorId: 2,
        requestAuthorUsername: 'bebrich#3162',
        requestType: 'Товар',
        requestPrice: 55,
        requestDescription:
          'Разнообразный и богатый опыт рамки и место обучения кадров способствует подготовки и реализации направлений прогрессивного развития',
        requestTime: '17:35 10.08.2021',
        status: 'Ожидание',
        performerId: 0,
        performerUsername: '',
        completed: false
      },
      {
        id: 3,
        authorId: 2,
        requestAuthorUsername: 'bebrich#3162',
        requestType: 'Услуга',
        requestPrice: 535,
        requestDescription: 'Разнообразный и богаправлений прогрессивного развития',
        requestTime: '13:35 10.08.2021',
        status: 'Ожидание',
        performerId: 0,
        performerUsername: '',
        completed: false
      },
      {
        id: 4,
        authorId: 42,
        requestAuthorUsername: 'hyperspace42#9872',
        requestType: 'Товар',
        requestPrice: 256,
        requestDescription: 'Разнообразный и богатый опыт рамессивного развития',
        requestTime: '17:16 10.07.2021',
        status: 'Выполнено',
        performerId: 2,
        performerUsername: 'bebrich#3162',
        completed: true
      },
    ],
  },

  getters: {
    getCurrentUser(state) {
      return state.currentUser;
    },

    getAllUsersRequests(state) {
      return state.allUsersRequests;
    },
  },

  mutations: {
    updateCurrentUserCash(state, {cash, freezedCash}) {
      state.currentUser.cash = cash
      state.currentUser.freezedCash = freezedCash
      state.allUsers.find((user) => user.id == state.currentUser.id).cash = cash
      state.allUsers.find((user) => user.id == state.currentUser.id).freezedCash = freezedCash
    },

    addUserRequest(state, userRequest) {
      state.allUsersRequests.unshift(userRequest);
    },

    deleteCurrentUserRequest(state, {requestId, cash, freezedCash}) {
      state.allUsersRequests = state.allUsersRequests.filter((request) => request.id != requestId);
      state.currentUser.cash = cash
      state.currentUser.freezedCash = freezedCash
      state.allUsers.find((user) => user.id == state.currentUser.id).cash = cash
      state.allUsers.find((user) => user.id == state.currentUser.id).freezedCash = freezedCash
    },

    addPerformerToRequest(state, {requestId, status, performerId, performerUsername, completed}) {
      state.allUsersRequests.find((request) => request.id == requestId).status = status
      state.allUsersRequests.find((request) => request.id == requestId).performerId = performerId
      state.allUsersRequests.find((request) => request.id == requestId).performerUsername = performerUsername
      state.allUsersRequests.find((request) => request.id == requestId).completed = completed
    },

    acceptRequest(state, {requestId, cash, freezedCash, performerId}) {

      state.currentUser.freezedCash = freezedCash
      state.allUsers.find((user) => user.id == state.currentUser.id).freezedCash = freezedCash
      state.allUsers.find((user) => user.id == performerId).cash += cash
      state.allUsersRequests = state.allUsersRequests.filter((request) => request.id != requestId);
    }
  },
});
